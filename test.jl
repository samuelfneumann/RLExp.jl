using RLExp
using RLAlgo
using RLAlgo.Deep
using RLAlgo.Buffer
using RLAlgo.Util.FluxUtil
using RLEnv.Environment
using RLEnv.Wrapper
using Random
using Flux
using RLCore


function main()
    env = StepLimit(Pendulum(Xoshiro(1); continuous=true), 1000)
    # env = StepLimit(Bimodal(), 1)
    env = ClipAction(env)
    rng = Xoshiro(1)
    in_ = size(observation_space(env))[1]

    π = LogitNormalPolicy(
        env,
        Chain(
            Dense(in_, 64; init = Flux.glorot_uniform(rng)),
            relu,
            Dense(64, 64; init = Flux.glorot_uniform(rng)),
            relu,
            Split(
                Dense(64, 1, identity; init = Flux.glorot_uniform),
                Dense(64, 1, softplus; init = Flux.glorot_uniform), # Required positive
            ),
        );
    )

    # π = LaplacePolicy(
    #     env,
    #     Chain(
    #         Dense(in_, 64; init = Flux.glorot_uniform(rng)),
    #         relu,
    #         Dense(64, 64; init = Flux.glorot_uniform(rng)),
    #         relu,
    #         Split(
    #             Dense(64, 1, identity; init = Flux.glorot_uniform),
    #             Chain(
    #                 Dense(64, 1, softplus; init = Flux.glorot_uniform),
    #                 x -> clamp.(x, 0.001f0, 1000f0), # Clamp the variance
    #             )
    #         ),
    #     );
    # )

    in_ += size(action_space(env))[1]
    q = Q([
        Chain(
            Concat(1),
            Dense(in_, 64; init = Flux.glorot_uniform),
            relu,
            Dense(64, 64; init = Flux.glorot_uniform),
            relu,
            Dense(64, 1; init = Flux.glorot_uniform),
        ),
    ])

    replay = ExperienceReplay(env, 10000)

    algo = RKL(
        env,
        Xoshiro(1);
        π = π,
        π_opt = Adam(0.0005),
        critic = q,
        critic_opt = Adam(0.001),
        replay = replay,
        batch_size = 32,
        τ = 1e-3,
        p = 0.01,
        reparameterized = false,
        baseline_actions = 29,
        num_samples = 1,
    )

    exp = Online(env, algo, StepEnder(20000))
    register!(exp, ReturnPerEpisode())
    run!(exp; verbose=true)
    out = RLCore.save(ReturnSaver(), exp)

    # exp = Online(env, algo, StepEnder(50000))
    # register!(exp, ReturnPerEpisode())
    # @time run!(exp; verbose=false)
    # out = RLCore.save(ReturnSaver(), exp)


    close!(exp)

end

function profile(algo, states, action, rewards, next_states, γs)
    Deep._update_critic!(algo, states, actions, rewards, next_states, γs)
end

main()
