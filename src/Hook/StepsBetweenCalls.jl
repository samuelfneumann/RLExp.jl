"""
	StepsBetweenCalls <: AbstractHook

Track the total number of steps per episode during an experiment.
"""
mutable struct StepsBetweenCalls <: AbstractHook
    _episode_steps::Vector{Int}
    _cumulative_steps::Int
	_name::String

    function StepsBetweenCalls(;name="steps_between_calls")
		return new([], 0, name)
    end
end

function (s::StepsBetweenCalls)(e::AbstractExperiment)
    push!(s._episode_steps, total_steps(e) - s._cumulative_steps)
    s._cumulative_steps = total_steps(e)
end

function RLCore.when(::StepsBetweenCalls)
	return PostEpisode
end

function RLCore.data(s::StepsBetweenCalls)
    return s._episode_steps
end

function RLCore.name(s::StepsBetweenCalls)::AbstractString
	return s._name
end

function Base.show(io::IO, s::StepsBetweenCalls)
    print(io, "StepsBetweenCalls: " * s._name)
end

RLCore.close!(s::StepsBetweenCalls, exp::AbstractExperiment) = nothing
