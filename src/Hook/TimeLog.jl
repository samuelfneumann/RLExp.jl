"""
	TimeLog <: AbstractHook

Log the time whenever called
"""
mutable struct TimeLog <: AbstractHook
    _when::CallTime
    _start_times::Vector{DateTime}
    _end_times::Vector{DateTime}
    _name::String
    _started::Bool

    function TimeLog(when ;name)
        return new(when, [], [], name, false)
    end
end

function (t::TimeLog)(::AbstractExperiment)
    if ! t._started
        push!(t._start_times, now())
        t._started = true
    else
        push!(t._end_times, now())
        t._started = false
    end
end

function RLCore.when(t::TimeLog)::CallTime
    return t._when
end

function RLCore.name(t::TimeLog)::AbstractString
    return t._name
end

function RLCore.data(t::TimeLog)::Vector{DateTime}
    return t._times
end

function Base.show(io::IO, t::TimeLog)
    print(io, "TimeLog:" * t._name)
end

function RLCore.close!(t::TimeLog, ::AbstractExperiment)
    if t._started
        push!(t._end_times, now())
    end
end
