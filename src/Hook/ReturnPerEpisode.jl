"""
	ReturnPerEpisode <: AbstractHook

Track the return per episode
"""
mutable struct ReturnPerEpisode{F} <: AbstractHook
    _episode_returns::Vector{F}
    _current_return::F
	_name::String

    function ReturnPerEpisode{F}(;name="episode_return") where {F<:Real}
        return new{F}([], 0.0, name)
    end
end

function ReturnPerEpisode(;name="episode_return")
    return ReturnPerEpisode{Float32}(name=name)
end

function (r::ReturnPerEpisode)(e::AbstractExperiment)
    if isterminal(env(e))
        r._current_return += reward(env(e))
        push!(r._episode_returns, r._current_return)
        r._current_return = 0.0
    else
        r._current_return += reward(env(e))
    end

    return nothing
end

function RLCore.when(::ReturnPerEpisode)
	return PostAct
end

function RLCore.data(r::ReturnPerEpisode)
    return r._episode_returns
end

function RLCore.name(r::ReturnPerEpisode)::AbstractString
	return r._name
end

function Base.show(io::IO, r::ReturnPerEpisode)
    print(io, "ReturnPerEpisode: " * r._name)
end

function RLCore.close!(r::ReturnPerEpisode, exp::AbstractExperiment)
    if !isterminal(env(exp))
        push!(r._episode_returns, r._current_return)
    end
end
