mutable struct EpisodeEnder <: AbstractExperimentEnder
    episode_limit::Int
end

function ended(ep_end::EpisodeEnder, e::AbstractExperiment)::Bool
    return ep_end.episode_limit <= total_episodes(e)
end
