mutable struct StepEnder <: AbstractExperimentEnder
    step_limit::Int
end

function ended(s::StepEnder, e::AbstractExperiment)::Bool
    return s.step_limit <= total_steps(e)
end

