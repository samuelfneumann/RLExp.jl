struct SerializationSaver <: AbstractExperimentSaver end

function RLCore.save(::SerializationSaver, e::AbstractExperiment, path::String)
    if !isdir(dirname(path))
        mkpath(dirname(path))
    end

    all_data = save(ReturnSaver(), e)

    serialize(path, all_data)
end
