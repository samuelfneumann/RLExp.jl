"""
	Online <: AbstractExperiment

An experiment that runs only online evaluation
"""
mutable struct Online{E,A} <: AbstractExperiment
    _env::E
    _algo::A
    _ender::AbstractExperimentEnder

    _hooks::Dict{CallTime, Vector{AbstractHook}}

    _state::AbstractArray
    _total_episodes::Int # How many episodes have currently completed
    _total_steps::Int    # How many steps have currently completed

    _copy_actions::Bool

    function Online(
            env::E,
            agent::A,
            ender::AbstractExperimentEnder,
            copy_actions::Bool = false,
    ) where {E<:AbstractEnvironment,A<:AbstractAlgo}
        # Set up the hook dictionary
        hooks = Dict{CallTime, Vector{AbstractHook}}()
        hooks[PreExperiment] = Vector{AbstractHook}()
        hooks[PreEpisode] = Vector{AbstractHook}()
        hooks[PreAct] = Vector{AbstractHook}()
        hooks[PostAct] = Vector{AbstractHook}()
        hooks[PostEpisode] = Vector{AbstractHook}()
        hooks[PostExperiment] = Vector{AbstractHook}()

        return new{E,A}(env, agent, ender, hooks, [], 0, 0, false)
    end
end

function RLCore.run!(o::Online; verbose=false)::Nothing
    RLCore.train!(o._algo)

    # Call pre experiment hooks
    for hook ∈ o._hooks[PreExperiment]
        hook(o)
    end

    # Run the experiment
    while !ended(o._ender, o)
        # Call pre episode hooks
        for hook ∈ o._hooks[PreEpisode]
            hook(o)
        end

        run_episode!(o; verbose=verbose)

        # Call post episode hooks
        for hook ∈ o._hooks[PostEpisode]
            hook(o)
        end
    end

    # Call post experiment hooks
    for hook ∈ o._hooks[PostExperiment]
        hook(o)
    end

    return nothing
end

function RLCore.run_episode!(o::Online; verbose=false)::Nothing
    reset!(o._algo)
    train!(o._algo)
    o._state = reset!(env(o))

    done = false
    if verbose
        ep_return = 0
        ep_steps = 0
        ep_start = time()
    end

    while !done && !ended(o._ender, o)
        # Call pre action hooks
        for hook ∈ o._hooks[PreAct]
            hook(o)
        end

        # Take an action
        action = select_action(o._algo, o._state)

        if o._copy_actions
            # The action is a vector which is passed by reference to the environment's step!
            # function. Since, the environment may alter the action, we pass a copy of the
            # action to the environment so that when calling step! on the algo, we use a
            # version of the action which was not altered by the env
            take = deepcopy(action)
            next_state, r, done, γ = step!(o._env, take)
        else
            next_state, r, done, γ = step!(o._env, action)
        end

        # Call post action hooks
        for hook ∈ o._hooks[PostAct]
            hook(o)
        end

        # Update the algo
        step!(o._algo, o._state, action, r, next_state, γ, done)

        if verbose
            ep_return += reward(env(o))
            ep_steps += 1
        end

        o._state = next_state
        o._total_steps += 1
    end

    if verbose
        ep_time = time() - ep_start
        println(
            "=== Train Ep $(o._total_episodes): ", ep_return, " ", ep_steps, " ", ep_time,
        )
    end

    o._total_episodes += 1

    return nothing
end

function RLCore.env(o::Online)::AbstractEnvironment
    return o._env
end

function RLCore.algo(o::Online)::AbstractAlgo
    return o._algo
end

function RLCore.data(o::Online)::NamedTuple
    out = Dict{Symbol, Any}()

    for call_time ∈ keys(hooks(o))
        map(hook -> out[Symbol(name(hook))] = data(hook), hooks(o)[call_time])
    end

    return NamedTuple(out)
end

function RLCore.register!(o::Online, t::AbstractHook)
    push!(o._hooks[when(t)], t)
end

function RLCore.hooks(o::Online)::Dict{CallTime, Vector{AbstractHook}}
    return o._hooks
end

function RLCore.total_steps(o::Online)::Int
    return o._total_steps
end

function RLCore.total_episodes(o::Online)::Int
    return o._total_episodes
end

RLCore.expclose!(::Online) = nothing
