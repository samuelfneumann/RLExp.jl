module RLExp

using Dates
using RLCore
using RLEnv

"""
    keysto(d::Dict{String,Any}[, T::Type = Symbol])::Dict{T,Any}

Convert the keys of a dictionary to T
"""
function keysto end

keysto() = Dict{Nothing,Any}()

function keysto(d::Dict, T::Type = Symbol)::Dict{T,Any}
    out = Dict{T,Any}()
    map(key -> out[T(key)] = d[key], collect(keys(d)))
    return out
end

# Enders
export EpisodeEnder, StepEnder
include("Ender/EpisodeEnder.jl")
include("Ender/StepEnder.jl")

# Hooks
export StepsBetweenCalls, ReturnPerEpisode, TimeLog
include("Hook/StepsBetweenCalls.jl")
include("Hook/ReturnPerEpisode.jl")
include("Hook/TimeLog.jl")

# Savers
export SerializationSaver, ReturnSaver
include("Saver/Serialization.jl")
include("Saver/Return.jl")

# Experiments
export Online
include("Online.jl")

end
